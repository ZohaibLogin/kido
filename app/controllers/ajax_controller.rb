class AjaxController < ApplicationController

	def handler
    call = "#{request.method}:#{params[:call]}"
    
    begin
      case call

      when 'GET:dn_home_data'

        url = "http://leafback.funworldpk.com/collection/#{params[:id_one]}/content?page=1&limit=10&telco=mobily"  
        resp_one = HTTParty.get(url)
        resp_one.to_json

        url = "http://leafback.funworldpk.com/collection/#{params[:id_two]}/content?page=1&limit=10&telco=mobily"  
        resp_two = HTTParty.get(url)
        resp_two.to_json
        
        combined_resp = []

        combined_resp[0] = resp_one
        combined_resp[1] = resp_two   

        ajax_success("Homepage collection data fetched", combined_resp)

      when 'GET:dn_collections'

        url = "http://leafback.funworldpk.com/categories?telco=mobily"  
        resp = HTTParty.get(url)
        resp.to_json   

        ajax_success("Collections data fetched", resp)

      when 'GET:dn_get_all_collections'

        url = "http://leafback.funworldpk.com/collection/list/all?page=1&limit=2"  
        resp = HTTParty.get(url)
        resp.to_json   

        ajax_success("All Collections data fetched", resp)

      end

      rescue Exception => e
      	ajax_fail(e.message)
      end

    end

end

private

def ajax_success(message, data = [], redirect_url=nil, redirect_delay=nil)
  resp = {
    success: true,
    message: message,
    data: data
  }
  ajax_resp(resp, redirect_url, redirect_delay, 200)
end

def ajax_fail(message, errors=[], redirect_url=nil, redirect_delay=nil)
  resp = {
    success: false,
    message: message,
    errors: errors,
  }
  ajax_resp(resp, redirect_url, redirect_delay, 200)
end

def ajax_resp(resp, redirect_url, redirect_delay, code)
  resp[:redirect_url]=redirect_url if redirect_url
  resp[:redirect_delay]=redirect_delay if redirect_delay
  render json: resp, status: code
end