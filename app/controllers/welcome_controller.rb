class WelcomeController < ApplicationController

# English Version
  def index
    url = "http://leafback.funworldpk.com/categories"
    @resp = HTTParty.get(url)
    @resp.to_json


  end

  def index_ar
  end

  def watch

    page_number = params[:page]

    puts "page number -> #{page_number}"

    url = "http://leafback.funworldpk.com/collection/list/all?page=" +  page_number.to_s  + "&limit=16"
    @resp = HTTParty.get(url)
    @resp.to_json
  end

  def watch_ar
  end

  def watch_season
    url = "http://leafback.funworldpk.com/api/videos/all_content?video_id=#{params[:id]}&limit=15"
    @resp = HTTParty.get(url)
    @resp.to_json
  end

  def watch_season_ar
  end

  def games

    page_number = params[:page]

    puts "page number -> #{page_number}"

    url = "http://leafback.funworldpk.com/games?limit=16&page=" + page_number.to_s

    @resp = HTTParty.get(url)
    @resp.to_json

  end

  def categories

    page_number = params[:page]

    category_id = params[:c]

    if category_id.nil?
      category_id = 3
    end
    if page_number.nil?
      page_number = 1
    end

    url = "http://leafback.funworldpk.com/categories/" + category_id.to_s + "?limit=12&page=" + page_number.to_s + "&telco=mobily"
    @resp = HTTParty.get(url)
    @resp.to_json

  end

  def abc_counting

    page_number = params[:page]

    puts "page number -> #{page_number}"

    url = "http://leafback.funworldpk.com/categories/3?limit=12&" + "page=" + page_number.to_s + "&telco=mobily"
    @resp = HTTParty.get(url)
    @resp.to_json

  end

  def rhymes_and_stories

    url = "http://leafback.funworldpk.com/categories/1?limit=12&page=1&telco=mobily"
    @resp = HTTParty.get(url)
    @resp.to_json

  end

  def science_and_fun

    url = "http://leafback.funworldpk.com/categories/4?limit=12&page=1&telco=mobily"
    @resp = HTTParty.get(url)
    @resp.to_json

  end

  def creative_thinking
    page_number = params[:page]

    puts "page number -> #{page_number}"

    url = "http://leafback.funworldpk.com/categories/2?limit=12&" + "page=" + page_number.to_s + "&telco=mobily"
    @resp = HTTParty.get(url)
    @resp.to_json
  end

  def toys
    page_number = params[:page]

    puts "page number -> #{page_number}"

    url = "http://leafback.funworldpk.com/categories/5?limit=12&" + "page=" + page_number.to_s + "&telco=mobily"
    @resp = HTTParty.get(url)
    @resp.to_json
  end

  def cartoons
    page_number = params[:page]

    puts "page number -> #{page_number}"

    url = "http://leafback.funworldpk.com/categories/6?limit=12&" + "page=" + page_number.to_s + "&telco=mobily"
    @resp = HTTParty.get(url)
    @resp.to_json
  end

  def nature
    page_number = params[:page]

    puts "page number -> #{page_number}"

    url = "http://leafback.funworldpk.com/categories/7?limit=12&" + "page=" + page_number.to_s + "&telco=mobily"
    @resp = HTTParty.get(url)
    @resp.to_json
  end

  def color_and_shapes
    page_number = params[:page]

    puts "page number -> #{page_number}"

    url = "http://leafback.funworldpk.com/categories/8?limit=12&" + "page=" + page_number.to_s + "&telco=mobily"
    @resp = HTTParty.get(url)
    @resp.to_json
  end


#Arabic Version
  def games_ar
  end

  def subscription
  end

  def subscription_ar
  end

  def categories_ar
  end

  def email
  end

  def email_ar
  end

end
