function bp_ajax(params) {
  var defaultParams = {
    url: '/ajax',
    action: '',
    method: 'get',
    contentType: 'application/json',
    data: {},
    spinnerEl: $('.bp-ajax-spinner'),
    spinner: true,
    notifications: true,
    notificationEl: $('.bp-notification-bar'),
    callback: $.noop,
    success: function(resp, jqXHR, textStatus) {
      params.spinner && params.spinnerEl && params.spinnerEl.length && params.spinnerEl.hide();
      if (typeof resp != 'object') return;
      if (params.notifications && params.notificationEl && params.notificationEl.length) {
        if (resp && resp.success && resp.message) bp_addNotification({parentEl: params.notificationEl, type: 'success', message: resp.message});
        else if (resp && !resp.success && resp.message) bp_addNotification({parentEl: params.notificationEl, type: 'error', message: resp.message});
      }
      
      if (resp.redirect_url && resp.redirect_delay) {
        setTimeout(function() { window.location.href = resp.redirect_url; }, resp.redirect_delay);
      }
      
      params.callback && params.callback(resp, jqXHR, textStatus);
        
    },
    error: function(jqXHR, textStatus, err) {
      params.spinner && params.spinnerEl && params.spinnerEl.length && params.spinnerEl.hide();
      //if (resp && !resp.success && params.notifications && params.notificationEL && params.notificationEL.length) bp_addNotification({parentEl: params.notificationEl, type: 'error', message: textStatus});
      params.callback && params.callback(err, jqXHR, textStatus);
    },
    beforeSend: function(jqXHR, textStatus) {}
  }
  params = $.extend(true, {}, defaultParams, params);
  
  params.spinner && params.spinnerEl && params.spinnerEl.length && params.spinnerEl.show();
  
  $.ajax({
    url: params.url + '/' + params.action,
    method: params.method,
    data: params.data,
    contentType: params.dataType,
    beforeSend: params.beforeSend,
    success: params.success,
    error: params.error,
  });
  
}


function bp_addNotification(params) {
  var defaultParams = {
    parentEl: $('.bp-notification-bar'),
    type: 'info',
    message: null,
    close: true,
    autoHide: false,
    autoHideTime: 0
  }
  
  params = $.extend(true, {}, defaultParams, params);
  if (!params.message) return null;
  
  var notificationEl = $('<div class="bp-notify"></div>'),
      classes = '',
      messageHTML = '';
  
  switch (params.type) {
    case 'danger':
    case 'error':
      classes += 'danger ';
      messageHTML += '<b>Error:</b> ';
      break;
      
    case 'warn':
    case 'warning':
      classes += 'warn ';
      messageHTML += '<b>Warning:</b> ';
      break;
      
    case 'info':
    case 'notify':
      classes += 'info ';
      messageHTML += '<b>Info:</b> ';
      break;
    
    case 'success':
    case 'successful':
      classes += 'succes ';
      messageHTML += '<b>Success:</b> ';
      break;
      
    default:
      classes += 'default ';
  }
  
  messageHTML += params.message;
  if (params.close) messageHTML += '<i class="i-icon-cancel-circled close"></i>';
  notificationEl.html(messageHTML).addClass(classes).appendTo(params.parentEl);
  $('i.close', notificationEl).click(function(e) {
    notificationEl.fadeOut();
  });
  return notificationEl;
}