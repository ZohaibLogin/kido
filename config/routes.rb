Rails.application.routes.draw do

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  #English Version routes

  root 'welcome#index',  as: :home

  get '/' => 'welcome#index'

  get '/games' => 'welcome#games'

  get '/categories'  => 'welcome#categories'

  get '/watch'  => 'welcome#watch'

  get '/watch_season' => "welcome#watch_season"

  get '/subscription'  => 'welcome#subscription'

  get '/email' => "welcome#email"

  # English version Categories routes

  get "/categories/abc_counting" => "categories#abc_counting"

  get '/rhymes_and_stories' => "welcome#rhymes_and_stories"

  get '/science_and_fun' => "welcome#science_and_fun"

  get '/toys' => "welcome#toys"

  get '/cartoons' => "welcome#cartoons"

  get '/nature' => "welcome#nature"

  get '/color_and_shapes' => "welcome#color_and_shapes"

  get '/creative_thinking' => "welcome#creative_thinking"

  # Arabic Version routes

  get '/index_ar' => 'welcome#index_ar'

  get '/games_ar' => 'welcome#games_ar'

  get '/categories_ar'  => 'welcome#categories_ar'

  get '/watch_ar'  => 'welcome#watch_ar'

  get '/subscription_ar'  => 'welcome#subscription_ar'

  get '/watch_season_ar' => "welcome#watch_season_ar"

  get '/email_ar' => "welcome#email_ar"

   # Ajax
  match '/ajax/:call' => 'ajax#handler', via: [:get, :put, :post, :delete], as: :ajax_action

end
